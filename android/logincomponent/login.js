/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    Alert,
    TouchableHighlight,
    TouchableOpacity,
    TextInput,
    ScrollView

} from 'react-native';
//const instructions = Platform.select({
//  ios: 'Press Cmd+R to reload,\n' +
//    'Cmd+D or shake for dev menu',
//  android: 'Double tap R on your keyboard to reload,\n' +
//    'Shake or press menu button for dev menu',
//});

type Props = {};


export default class login extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = { text1: 'enter your email...' , text2: 'enter your password...' };
    }
    _onPressButton() {
        console.log('button works');
    }

    render() {

        return (
            <ScrollView style={styles.container}>
                <View style={styles.firstBox}>
                    <Image style={styles.brandLogo} source={require('../../assets/img/logo.jpg')} />
                    <Text style={styles.header}>Welcome To Hubnsub</Text>
                    <Text style={styles.title}>Login to your account</Text>
                </View>
                <View style={styles.secondBox}>
                    <TextInput
                        style={styles.materialInput}
                        onChangeText={(text1) => this.setState({text1})}
                        value={this.state.text1}
                    />
                    <TextInput
                        style={styles.materialInput}
                        onChangeText={(text2) => this.setState({text2})}
                        value={this.state.text2}
                    />
                    <TouchableHighlight
                        onPress={this._onPressButton}
                        underlayColor='#402038'
                        style={styles.buttonLogin}>
                        <Text style={styles.buttonText}> Login </Text>
                    </TouchableHighlight>
                </View>
                <View style={styles.thirdBox}>
                    <Text>Not a member yet?</Text>
                    <TouchableHighlight
                        onPress={this._onPressButton}
                        underlayColor='#ffffff'>
                        <Text style={styles.btnRegister}>CREATE ACCOUNT</Text>
                    </TouchableHighlight>


                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        height: '100%',
        width:'100%',
        backgroundColor: '#ffffff'
    },
    navBar: {
        height:55,
        elevation: 3,
        flex: 1,
        backgroundColor: '#ffffff',
    },
    brandLogo: {
        width:'100%',
        height:70,
        overflow:'visible'
    },
    firstBox: {
        flex:1,
        flexDirection: 'column',
        justifyContent:'center',
        flex: 1,
        marginTop:40,
        marginBottom:10,
        flexGrow:2,

    },
    secondBox:{
        flexDirection: 'column',
        justifyContent:'center',
        alignItems: 'center',
        flex: 1,
    },
    thirdBox:{
        flex:1,
        flexDirection: 'row',
        justifyContent:'space-between',
        marginBottom: 90,
        padding:10,
        marginTop:10,

    },
    btnRegister:{
        color: '#402038',
        textAlign:'right'
    },
    header: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 40,
    },
    title: {
        fontSize: 15,
        textAlign: 'center',
    },
    materialInput: {
        height: 40,
        borderColor: 'gray',
        borderBottomWidth: 0,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        width:300,
    },
    buttonLogin: {
        padding: 8,
        width: 300,
        height: 50,
        marginTop: 20,
        justifyContent: 'center',
        backgroundColor: '#744268',
    },
    buttonText:{
        fontSize: 15,
        textAlign: 'center',
        color: '#fff',

    }

});

